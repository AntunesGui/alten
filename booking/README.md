  <h3 align="center">README</h3>

  <p align="center">
    Here you will find a brief description of the operation and configuration of the project.
    </p>
    
<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
This project is an API to help the last hotel in Cancun to make reservations. We provide the service 
in addition to making the reservation, it is possible to cancel, update and search for availability, 
these benefits are achieved through our endpoints.

Although we have some rules to be follow, to be able to proceed with the reservation such as:

* The stay can’t be longer than 3 days.
* Can’t be reserved more than 30 days in advance.
* Every end-user can check the room availability, place a reservation, cancel it or modify it.
* All reservations start at least the next day of booking. 
* All customers are able to make reservation in any period of the month, however, the sum of these 
days can't longer than 3 days.
* After 30 days, customers are able to make a new reservation, however, the rules described above must be met.
* Customer cannot select a check-in date less than the current date.
* Customer cannot select a check-in date and check-out at the same day.
* Customer cannot select a check-out date less than the current date.


It is important to say that, the values ​​of days corresponding to topic one and two, are parameterized in 
"application.properties" file, which makes system maintenance easier.

### Built With

In this project we use some famous and renowned technologies, such as:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [MySQL](https://www.mysql.com/)
* [Maven](https://maven.apache.org/)
* [Docker](https://www.docker.com/)
* [Java](https://www.java.com/pt-BR/)
* [SLF4J](http://www.slf4j.org/)
* [Lombok](https://projectlombok.org/)
* [Swagger](https://swagger.io/)
* [Mockito](https://site.mockito.org/)
* [JUnit](https://junit.org/junit5/)
* [Jacoco](https://www.baeldung.com/jacoco)
* [Actuator](https://spring.io/guides/gs/actuator-service/)

About the project structure, we chose to build this project using the Clean architecture due to its benefits:

* Framework Independent – You can use clean architecture with ASP.NET (Core), Java, Python, etc. It doesn’t rely on any software library or proprietary codebase.
* Database Independent-Majority of your code has no knowledge of what database, if any, is being used by the application. Often, this info will exist in a single class, in a single project that no other project references.
* UI Independent-The UI project cares about the UI only. It has nothing to do with the implementation of business or data logic.
* Highly Testable– Apps built using this approach, and especially the core domain model and its business rules, are extremely testable.

[Clean architecture] https://www.baeldung.com/spring-boot-clean-architecture

Bringing more details of the organization of the project packages

* Domain: Is a set of related business rules that are critical to the 
function of the application. In an object oriented programming language the 
rules for an entity would be grouped together as methods in a class. 
Even if there were no application, these rules would still exist.

* Use cases: The use cases are the business rules for a 
specific application. They tell how to automate the system. 
This determines the behavior of the app. Package "port": Note, that the 
interface is also known as a port, as it makes the bridge between the business 
logic and the outside world.

* Boudary: The boudary, also called interface adapters, 
are the translators between the domain and the infrastructure. 
For example, they take input data from the GUI and repackage it in a form that 
is convenient for the use cases and entities. Then they take the output from 
the use cases and entities and repackage it in a form that is convenient for 
displaying in the GUI or saving in a database.

* Configuration: The configuration has the responsibility to manage application 
settings such as database settings, log configuration. In our case, we are using it 
for swagger configuration.

* Exception: The exception has a responsibility to maintain our entities to 
handle system exceptions.

<!-- GETTING STARTED -->
## Getting Started

In order to run the project, it is necessary to meet and follow the requirements below:

### Prerequisites

First of all, you need to clone the project, from gitlab.
* 
  ```sh
   JDK 11
   Docker
   Postman
   IntelliJ
   ```
### Installation

1. Install Docker Engine and Docker Desktop
   ```sh
   https://docs.docker.com/engine/install/ubuntu/
   https://www.docker.com/products/docker-desktop
   ```
2. Clone the repo
   ```sh
   git clone git@gitlab.com:AntunesGui/alten.git
   ```
3. Install JDK 11
   ```sh
   https://www.oracle.com/br/java/technologies/javase-jdk11-downloads.html
   ```
4. Install IntelliJ
   ```sh
    https://www.jetbrains.com/idea/download/#section=mac
   ```
5. Import the project to your IntelliJ IDE
   ```sh
     https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html
   ```
6. Setup SDK to your IntelliJ IDE
   ```sh
     https://www.jetbrains.com/help/idea/sdk.html
   ```
7. Run docker-compose, execute command in IDE terminal, docker will start MySQL database.
   ```sh
     docker-compose up -d
   ```
8. Run application:
* Open the BookingApplication.class -> right click -> Run BookingApplication.class

<!-- USAGE EXAMPLES -->
## Usage

After finished all the necessary configuration to start the application, you are able to use 
the Postman collection, that is at the root of the project with the named "Alten_Collection"

 - After the application is up, it will be possible to consult the documentation of the application's 
 endpoints, through the swagger that we have implemented, with the link below it is possible to view it through the 
 browser.
 
   ```sh
     http://localhost:9091/swagger-ui.html#/book-controller
   ```
  - You are able to check the current coverage ratio from the project, bellow there is a example path where jacoco-pluin
  will send the file "index.html". Currently code coverage is 84%.
    
     ```sh
       file:///Users/Gui/Documents/alten-project/alten/booking/target/site/jacoco/index.html
     ```
      
 - How to import a collection into Postman:
   ```sh
     https://kb.datamotion.com/?ht_kb=postman-instructions-for-exporting-and-importing
   ```
 - Each request has its proper description of what will be executed, as well as support, there are 
 examples prepared to start the tests.
 
 - To test a reservation after thirty days, you need have a first reservation and then, access the database with 
 user: root and password: root and insert the command bellow as example:
    ```sh
    update booking set check_in="2021-07-28", check_out="2021-07-29" where booking_id=3;
    ```
 After that, it's just send a request with same user sin, new valid check-in and check-out date.

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request

<!-- CONTACT -->
## Contact

Guilherme de Oliveira Antunes - [@Gmail](https://twitter.com/your_username) - antunes.guioliveira@gmail.com

Project Link: [https://gitlab.com/AntunesGui/alten](https://gitlab.com/AntunesGui/alten)

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew
[product-screenshot]: images/screenshot.png