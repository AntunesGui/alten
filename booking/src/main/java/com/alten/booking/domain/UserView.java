package com.alten.booking.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserView {

    private Long userId;

    private List<BookingView> booking;

    private String sin;

    private String name;

    private String email;
}
