package com.alten.booking.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.time.LocalDate;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookingView {

    @JsonProperty("booking_id")
    private Long bookingId;

    @JsonProperty("check_in")
    private LocalDate checkIn;

    @JsonProperty("check_out")
    private LocalDate checkOut;

    @JsonProperty("bedroom")
    private BedRoomView bedroom;

    @JsonProperty("user")
    private UserView user;
}
