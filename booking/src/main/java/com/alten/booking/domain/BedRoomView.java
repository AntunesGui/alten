package com.alten.booking.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BedRoomView {

    @JsonProperty("bedroom_id")
    private Long bedroomId;
}
