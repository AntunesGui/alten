package com.alten.booking.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class User {

    @NotBlank(message = "Name is mandatory")
    @Valid
    @JsonProperty("name")
    private String name;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    @JsonProperty("bith_day")
    private Date birthDay;

    @NotBlank(message = "SIN number is mandatory")
    @Valid
    @JsonProperty("sin")
    private String sin;

    @JsonProperty("email")
    private String email;
}
