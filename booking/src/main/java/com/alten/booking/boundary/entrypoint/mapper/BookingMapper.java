package com.alten.booking.boundary.entrypoint.mapper;

import com.alten.booking.boundary.dataprovider.entity.BookingEntity;
import com.alten.booking.domain.BookingView;
import com.alten.booking.domain.UserView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@AllArgsConstructor
@Component
public class BookingMapper {

    public BookingView from(BookingEntity bookingEntity) {

        return mapBookingView(bookingEntity);
    }

    private BookingView mapBookingView(BookingEntity bookingEntity) {

        return BookingView.builder()
                .bookingId(bookingEntity.getBookingId())
                .checkIn(bookingEntity.getCheckIn())
                .checkOut(bookingEntity.getCheckOut())
                .user(UserView.builder()
                        .userId(bookingEntity.getUser().getUserId())
                        .name(bookingEntity.getUser().getName())
                        .sin(bookingEntity.getUser().getSin())
                        .email(bookingEntity.getUser().getEmail())
                        .build())
                .build();
    }
}
