package com.alten.booking.boundary.dataprovider.entity;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReservationEntity {

    private UserEntity userEntity;

    private BookingEntity bookingEntity;
}
