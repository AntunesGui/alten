package com.alten.booking.boundary.entrypoint.dto;

import com.alten.booking.domain.User;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDto extends User {
}
