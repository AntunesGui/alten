package com.alten.booking.boundary.entrypoint.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReservationDto {

    @Valid
    @NotNull
    @JsonProperty("customer")
    private CustomerDto customerDto;

    @Valid
    @NotNull
    @JsonProperty("booking")
    private BookingDto bookingDto;
}
