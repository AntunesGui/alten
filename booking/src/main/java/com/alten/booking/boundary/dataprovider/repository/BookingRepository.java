package com.alten.booking.boundary.dataprovider.repository;

import com.alten.booking.boundary.dataprovider.entity.BookingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Long> {

    @Query("SELECT COUNT(*) FROM booking B " +
            "WHERE :checkIn >= B.checkIn AND " +
            ":checkIn <= B.checkOut OR " +
            ":checkOut >= B.checkIn " +
            "AND :checkOut <= B.checkOut")
    Integer checkAllAvailableReservations(@Param("checkIn") LocalDate checkIn, @Param("checkOut") LocalDate checkOut);
}
