package com.alten.booking.boundary.dataprovider.repository;

import com.alten.booking.boundary.dataprovider.entity.AltenHotelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AltenHotelRepository extends JpaRepository<AltenHotelEntity, Long> {

}
