package com.alten.booking.boundary.entrypoint.mapper;

import com.alten.booking.boundary.dataprovider.entity.BookingEntity;
import com.alten.booking.boundary.dataprovider.entity.ReservationEntity;
import com.alten.booking.boundary.dataprovider.entity.UserEntity;
import com.alten.booking.boundary.entrypoint.dto.ReservationDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Getter
@AllArgsConstructor
@Component
public class ReservationMapper {

    private final ModelMapper mapper;

    public ReservationEntity from(ReservationDto reservationDto) {

        return mapReservationEntity(reservationDto);
    }

    private ReservationEntity mapReservationEntity(ReservationDto reservationDto) {

        ReservationEntity reservationEntity = new ReservationEntity();

        reservationDto.getBookingDto().setCheckIn(reservationDto.getBookingDto().getCheckIn().plusDays(1));
        reservationDto.getBookingDto().setCheckOut(reservationDto.getBookingDto().getCheckOut().plusDays(1));

        reservationEntity.setBookingEntity(mapper.map(reservationDto.getBookingDto(), BookingEntity.class));
        reservationEntity.setUserEntity(mapper.map(reservationDto.getCustomerDto(), UserEntity.class));

        return reservationEntity;
    }
}
