package com.alten.booking.boundary.dataprovider.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "alten_hotel")
public class AltenHotelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "alten_hotel_id")
    private Long altenHotelId;

    @OneToOne(mappedBy = "altenHotel", fetch = FetchType.EAGER)
    private BedRoomEntity bedroom;

}