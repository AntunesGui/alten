package com.alten.booking.boundary.dataprovider.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity(name = "bedroom")
public class BedRoomEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bedroom_id")
    private Long bedroomId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "alten_hotel_id", referencedColumnName = "alten_hotel_id")
    private AltenHotelEntity altenHotel;

    @OneToMany(mappedBy = "bedroom")
    private List<BookingEntity> booking;
}
