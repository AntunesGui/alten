package com.alten.booking.boundary.entrypoint.mapper;

import com.alten.booking.boundary.dataprovider.entity.UserEntity;
import com.alten.booking.domain.BookingView;
import com.alten.booking.domain.UserView;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Getter
@AllArgsConstructor
@Component
public class UserMapper {

    public UserView from(UserEntity userEntity) {

        return mapUserView(userEntity);
    }

    private UserView mapUserView(UserEntity userEntity) {

        final List<BookingView> bookingViewList = new ArrayList<>();

        userEntity.getBooking().forEach(bookingEntity -> bookingViewList.add(BookingView.builder()
                .bookingId(bookingEntity.getBookingId())
                .checkIn(bookingEntity.getCheckIn())
                .checkOut(bookingEntity.getCheckOut())
                .build()));

        return UserView.builder()
                .userId(userEntity.getUserId())
                .name(userEntity.getName())
                .sin(userEntity.getSin())
                .booking(bookingViewList)
                .build();
    }
}
