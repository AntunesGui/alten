package com.alten.booking.boundary.dataprovider.repository;

import com.alten.booking.boundary.dataprovider.entity.BedRoomEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BedRoomRepository extends JpaRepository<BedRoomEntity, Long> {

}
