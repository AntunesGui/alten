package com.alten.booking.boundary.dataprovider.implementation;

import com.alten.booking.boundary.dataprovider.entity.*;
import com.alten.booking.boundary.dataprovider.repository.AltenHotelRepository;
import com.alten.booking.boundary.dataprovider.repository.BedRoomRepository;
import com.alten.booking.boundary.dataprovider.repository.BookingRepository;
import com.alten.booking.boundary.dataprovider.repository.UserRepository;
import com.alten.booking.boundary.entrypoint.mapper.BookingMapper;
import com.alten.booking.boundary.entrypoint.mapper.UserMapper;
import com.alten.booking.domain.BookingView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.usecases.BookingRuleService;
import com.alten.booking.usecases.port.BookingCrudRepositoryPort;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@AllArgsConstructor
@Repository
public class BookingCrudRepositoryImp implements BookingCrudRepositoryPort {

    private final UserMapper userMapper;
    private final BookingMapper bookingMapper;
    private final UserRepository userRepository;
    private final BedRoomRepository bedRoomRepository;
    private final BookingRepository bookingRepository;
    private final BookingRuleService bookingRuleService;
    private final AltenHotelRepository altenHotelRepository;

    @Override
    public BookingView save(ReservationEntity reservationEntity) throws BookingRuleException {

        log.info("Retrieve customer information by sin = {}", reservationEntity.getUserEntity().getSin());
        var userEntityResponse = userRepository.findBySin(reservationEntity.getUserEntity().getSin());

        if (userEntityResponse.isPresent() && !userEntityResponse.get().getBooking().isEmpty()) {

            var userView = userMapper.from(userEntityResponse.get());

            if (bookingRuleService.checkIfTheFirstCheckInIsAfterThirtyDays(userView.getBooking().get(0))) {

                return checkinAfterThirtyDays(userEntityResponse.get(), reservationEntity);
            }
            return userAlreadyHasCheckinOrRegistered(reservationEntity, userEntityResponse.get());
        }
        return makeFirstReservation(reservationEntity);
    }

    @Transactional
    private BookingView makeFirstReservation(ReservationEntity reservationEntity) {

        AltenHotelEntity altenHotelEntity = new AltenHotelEntity();
        BedRoomEntity bedRoomEntity = new BedRoomEntity();

        AltenHotelEntity savedAltenHotel = altenHotelRepository.save(altenHotelEntity);
        bedRoomEntity.setAltenHotel(savedAltenHotel);

        UserEntity savedUser = userRepository.save(reservationEntity.getUserEntity());
        BedRoomEntity savedBedroom = bedRoomRepository.save(bedRoomEntity);

        reservationEntity.getBookingEntity().setUser(savedUser);
        reservationEntity.getBookingEntity().setBedroom(savedBedroom);

        BookingEntity savedBookingEntity = bookingRepository.save(reservationEntity.getBookingEntity());

        return bookingMapper.from(savedBookingEntity);
    }

    @Override
    @Transactional
    public void delete(Long bookingId) {

        bookingRepository.deleteById(bookingId);
    }

    @Override
    @Transactional
    public void update(ReservationEntity reservationEntity, Long bookingId) throws BookingException {

        var bookingEntityToUpdate = bookingRepository.getById(bookingId);
        bookingEntityToUpdate.setCheckIn(reservationEntity.getBookingEntity().getCheckIn());
        bookingEntityToUpdate.setCheckOut(reservationEntity.getBookingEntity().getCheckOut());
        bookingRepository.save(bookingEntityToUpdate);
    }

    @Override
    public List<BookingView> getAllReservations(Pageable pageable) {

        Page<BookingEntity> bookingEntities = bookingRepository.findAll(pageable);
        final List<BookingView> bookingViewList = new ArrayList<>();

        bookingEntities.forEach(bookingEntity -> bookingViewList.add(
                BookingView.builder()
                        .bookingId(bookingEntity.getBookingId())
                        .checkIn(bookingEntity.getCheckIn())
                        .checkOut(bookingEntity.getCheckOut())
                        .build()));

        return bookingViewList;
    }

    @Transactional
    private BookingView userAlreadyHasCheckinOrRegistered(ReservationEntity reservationEntity, UserEntity userEntityResponse) throws
            BookingRuleException {

        bookingRuleService.checkTotalDaysReserved(userMapper.from(userEntityResponse), reservationEntity);

        var savedBedroom = bedRoomRepository.findById(userEntityResponse.getUserId());

        reservationEntity.getBookingEntity().setUser(userEntityResponse);
        reservationEntity.getBookingEntity().setBedroom(savedBedroom.get());
        return bookingMapper.from(bookingRepository.save(reservationEntity.getBookingEntity()));
    }

    @Override
    @Transactional
    public void verifyAvailability(ReservationEntity reservationEntity) throws BookingException {

        var fullDates = bookingRepository.checkAllAvailableReservations(
                reservationEntity.getBookingEntity().getCheckIn(),
                reservationEntity.getBookingEntity().getCheckOut());

        log.info("Check all available reservation by checkin = {} and checkout = {}",
                reservationEntity.getBookingEntity().getCheckIn(),
                reservationEntity.getBookingEntity().getCheckOut());

        if (fullDates > 0)
            throw new BookingException("Opps! Sorry, but this period is no longer available, please try another date");
    }

    private BookingView checkinAfterThirtyDays(UserEntity userEntity, ReservationEntity reservationEntity) throws BookingRuleException {

        log.info("Canceling/deleting reservation by sin = {}", reservationEntity.getUserEntity().getSin());

        delete(userEntity.getBooking().get(0).getBookingId());
        userEntity.getBooking().remove(0);
        return userAlreadyHasCheckinOrRegistered(reservationEntity, userEntity);
    }
}

