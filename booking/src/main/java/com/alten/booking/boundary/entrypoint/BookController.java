package com.alten.booking.boundary.entrypoint;

import com.alten.booking.boundary.entrypoint.dto.ReservationDto;
import com.alten.booking.boundary.entrypoint.mapper.ReservationMapper;
import com.alten.booking.domain.BookingView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.usecases.BookingRuleService;
import com.alten.booking.usecases.BookingService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/alten-hotel")
public class BookController {

    private final BookingService bookingService;
    private final ReservationMapper reservationMapper;
    private final BookingRuleService bookingRuleService;

    @PostMapping(path = "/book/reservation/rooms")
    @ResponseStatus(code = HttpStatus.CREATED)
    public BookingView makeReservation(@RequestBody @Valid ReservationDto reservationDto) throws BookingRuleException, BookingException {

        bookingRuleService.bookingRule(reservationDto.getBookingDto());
        var reservationEntityMapper = reservationMapper.from(reservationDto);
        return bookingService.makingReservation(reservationEntityMapper);
    }

    @DeleteMapping("book/reservation/{bookingId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void cancelReservation(@PathVariable("bookingId") Long bookingId) {

        bookingService.cancelReservation(bookingId);
    }

    @PutMapping("/book/reservation/{bookingId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void updateReservation(
            @RequestBody @Valid ReservationDto reservationDto,
            @PathVariable("bookingId") Long bookingId) throws BookingRuleException, BookingException {

        bookingRuleService.bookingRule(reservationDto.getBookingDto());
        var reservationEntityMapper = reservationMapper.from(reservationDto);
        bookingService.updateReservation(reservationEntityMapper, bookingId);
    }

    @GetMapping("/book/list")
    @ResponseStatus(code = HttpStatus.OK)
    public Page checkAllAvailableReservations(Pageable pageable) {

        List<BookingView> bookingViewList = bookingService.getAllReservations(pageable);
        return new PageImpl<>(bookingViewList, pageable, bookingViewList.size());
    }
}
