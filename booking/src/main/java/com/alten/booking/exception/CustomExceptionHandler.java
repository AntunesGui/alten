package com.alten.booking.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.ZonedDateTime;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(BookingRuleException.class)
    @ResponseStatus(BAD_REQUEST)
    protected ExceptionDetails handlerBookingRuleException(BookingRuleException exception) {

        return ExceptionDetails.builder()
                .timesTamp(ZonedDateTime.now())
                .statusCode(BAD_REQUEST.value())
                .message(exception.getMessage())
                .build();
    }

    @ExceptionHandler(BookingException.class)
    @ResponseStatus(BAD_REQUEST)
    protected ExceptionDetails handlerBookingException(BookingException exception) {

        log.info("Empty result data exception, with message: {}", exception.getMessage());
        return ExceptionDetails.builder()
                .timesTamp(ZonedDateTime.now())
                .statusCode(BAD_REQUEST.value())
                .message(exception.getMessage())
                .build();
    }

    @ExceptionHandler(EmptyResultDataAccessException.class)
    @ResponseStatus(NOT_FOUND)
    protected ExceptionDetails handlerEmptyResultDataAccessException(EmptyResultDataAccessException exception) {

        log.error("Empty result data exception, with message: {}", exception.getMessage());
        return ExceptionDetails.builder()
                .timesTamp(ZonedDateTime.now())
                .statusCode(NOT_FOUND.value())
                .message("Oops, this data does not exist")
                .build();
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    protected ExceptionDetails handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {

        log.error("Payload is not valid, details from exception: {}", exception.getMessage());
        return ExceptionDetails.builder()
                .timesTamp(ZonedDateTime.now())
                .statusCode(BAD_REQUEST.value())
                .message("Oops, i think you forgot to fill in some field")
                .build();
    }

    @ExceptionHandler(JpaObjectRetrievalFailureException.class)
    @ResponseStatus(NOT_FOUND)
    protected ExceptionDetails handleJpaObjectRetrievalFailureException(JpaObjectRetrievalFailureException exception) {

        log.error("BookingId not found to update reservation, details message: {}", exception.getMessage());
        return ExceptionDetails.builder()
                .timesTamp(ZonedDateTime.now())
                .statusCode(NOT_FOUND.value())
                .message("Oops, this data does not exist")
                .build();
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    protected ExceptionDetails handleUnexpectedExceptions(Exception exception) {

        log.error("Unexpected exception, with message: {}", exception.getMessage());
        return ExceptionDetails.builder()
                .timesTamp(ZonedDateTime.now())
                .statusCode(INTERNAL_SERVER_ERROR.value())
                .message("Oops, we had an internal problem in our system")
                .build();
    }
}
