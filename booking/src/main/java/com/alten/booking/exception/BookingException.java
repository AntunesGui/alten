package com.alten.booking.exception;

import lombok.Getter;

@Getter
public class BookingException extends Exception {

    public BookingException(final String message) {
        super(message);
    }
}
