package com.alten.booking.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.ZonedDateTime;

@Builder
@Getter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExceptionDetails {

    private ZonedDateTime timesTamp;
    private Integer statusCode;
    private String detail;
    private String message;
}
