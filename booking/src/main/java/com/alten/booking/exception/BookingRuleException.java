package com.alten.booking.exception;

import lombok.Getter;

@Getter
public class BookingRuleException extends Exception {

    public BookingRuleException(final String message) {
        super(message);
    }
}
