package com.alten.booking.usecases;

import com.alten.booking.boundary.dataprovider.entity.ReservationEntity;
import com.alten.booking.domain.BookingView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.usecases.port.BookingCrudRepositoryPort;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class BookingService {

    private final ModelMapper mapper;

    private final BookingCrudRepositoryPort repositoryPort;

    public BookingView makingReservation(ReservationEntity reservationEntity) throws BookingException, BookingRuleException {

        repositoryPort.verifyAvailability(reservationEntity);
        return repositoryPort.save(reservationEntity);
    }

    public List<BookingView> getAllReservations(Pageable pageable) {

        List<BookingView> bookingList = repositoryPort.getAllReservations(pageable)
                .stream()
                .map(bookingView -> mapper.map(bookingView, BookingView.class))
                .collect(Collectors.toList());
        return bookingList;
    }

    public void cancelReservation(Long bookingId) {

        repositoryPort.delete(bookingId);
    }

    public void updateReservation(ReservationEntity reservationEntity, Long bookingId) throws BookingException {

        repositoryPort.verifyAvailability(reservationEntity);
        repositoryPort.update(reservationEntity, bookingId);
    }
}
