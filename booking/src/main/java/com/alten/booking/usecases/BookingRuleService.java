package com.alten.booking.usecases;

import com.alten.booking.boundary.dataprovider.entity.ReservationEntity;
import com.alten.booking.boundary.entrypoint.dto.BookingDto;
import com.alten.booking.domain.BookingView;
import com.alten.booking.domain.UserView;
import com.alten.booking.exception.BookingRuleException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import static java.lang.Boolean.*;

@NoArgsConstructor
@Slf4j
@Service
public class BookingRuleService {

    @Value("${booking.rules.days.longer}")
    private Integer daysCantBeLonger;

    @Value("${booking.rules.reserve.advance}")
    private Integer daysCantBeReservedInAdvance;

    public void bookingRule(BookingDto bookingDto) throws BookingRuleException {

        verifyCheckOutIsEqualOrShorterThanCheckIn(bookingDto);
        verifyStayNotLongerThanTheTimeAllowed(bookingDto);
        verifyStayCantReservedInAdvance(bookingDto);
        verifyCheckinIsShorterAndEqualsTodaysDate(bookingDto);
    }

    private void verifyCheckOutIsEqualOrShorterThanCheckIn(BookingDto bookingDto) throws BookingRuleException {

        if (bookingDto.getCheckOut().isBefore(bookingDto.getCheckIn()) || bookingDto.getCheckOut().isEqual(bookingDto.getCheckIn())) {
            log.error("Oops! The check-out can’t be equal or shorter than check-in");
            throw new BookingRuleException("Ops! The check-out can’t be equal or shorter than check-in");
        }
    }

    private void verifyStayNotLongerThanTheTimeAllowed(BookingDto bookingDto) throws BookingRuleException {

        if (bookingDto.getCheckOut().isAfter(bookingDto.getCheckIn().plusDays(daysCantBeLonger))) {
            log.error("Oops! The stay can’t be longer than 3 days");
            throw new BookingRuleException("Ops! The stay can’t be longer than 3 days");
        }
    }

    private void verifyStayCantReservedInAdvance(BookingDto bookingDto) throws BookingRuleException {

        if (bookingDto.getCheckIn().isAfter(LocalDate.now().plusDays(daysCantBeReservedInAdvance))) {
            log.error("Oops! The Stay can’t be reserved more than 30 days in advance");
            throw new BookingRuleException("Ops! The Stay can’t be reserved more than 30 days in advance");
        }
    }

    private void verifyCheckinIsShorterAndEqualsTodaysDate(BookingDto bookingDto) throws BookingRuleException {

        if (!bookingDto.getCheckIn().isAfter(LocalDate.now()) && !bookingDto.getCheckIn().isEqual(LocalDate.now())) {
            log.error("Oops! Invalid check-in date");
            throw new BookingRuleException("Ops! Invalid check-in date");
        }
    }

    public void checkTotalDaysReserved(UserView userView, ReservationEntity reservationEntity) throws BookingRuleException {

        Long reserverdDays = 0L;
        for (BookingView bookingView : userView.getBooking()) {
            reserverdDays += ChronoUnit.DAYS.between(bookingView.getCheckIn(), bookingView.getCheckOut());
            if (reserverdDays >= daysCantBeLonger) {
                log.error("Already booked the maximum amount of days");
                throw new BookingRuleException("Sorry, you still can not book more than 3 days with us");
            }
        }
        var response = reserverdDays + ChronoUnit.DAYS.between(reservationEntity.getBookingEntity().getCheckIn(), reservationEntity.getBookingEntity().getCheckOut());
        if (response > daysCantBeLonger)
            throw new BookingRuleException("Sorry, you still can not book more than 3 days with us");
    }

    public Boolean checkIfTheFirstCheckInIsAfterThirtyDays(BookingView bookingView) {

        if (bookingView.getCheckIn().plusDays(daysCantBeReservedInAdvance).isBefore(LocalDate.now()) ||
                bookingView.getCheckIn().plusDays(daysCantBeReservedInAdvance).isEqual(LocalDate.now())) {
            log.info("Customer is able to make a new reservation");
            return TRUE;
        }
        return FALSE;
    }
}
