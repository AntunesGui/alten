package com.alten.booking.usecases.port;

import com.alten.booking.boundary.dataprovider.entity.ReservationEntity;
import com.alten.booking.domain.BookingView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookingCrudRepositoryPort {

    BookingView save(ReservationEntity reservationEntity) throws BookingRuleException;

    void delete(Long bookingId);

    void update(ReservationEntity reservationEntity, Long bookingId) throws BookingException;

    List<BookingView> getAllReservations(Pageable pageable);

    void verifyAvailability(ReservationEntity reservationEntity) throws BookingException;
}
