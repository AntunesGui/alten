package com.alten.booking.utils;

import com.alten.booking.boundary.dataprovider.entity.*;
import com.alten.booking.boundary.entrypoint.dto.BookingDto;
import com.alten.booking.boundary.entrypoint.dto.CustomerDto;
import com.alten.booking.boundary.entrypoint.dto.ReservationDto;
import com.alten.booking.domain.BedRoomView;
import com.alten.booking.domain.BookingView;
import com.alten.booking.domain.UserView;

import java.time.LocalDate;
import java.util.List;

public class Utils {

    public static BookingDto buildBookingDtoCheckOutGreaterThanCheckIn() {

        return BookingDto.builder()
                .checkIn(LocalDate.now())
                .checkOut(LocalDate.now().minusDays(2))
                .build();
    }

    public static BookingDto buildBookingDtoCheckOutEqualCheckIn() {

        return BookingDto.builder()
                .checkIn(LocalDate.now())
                .checkOut(LocalDate.now())
                .build();
    }

    public static BookingDto buildBookingDtoCheckOutLongerThanAllowed() {

        return BookingDto.builder()
                .checkIn(LocalDate.now())
                .checkOut(LocalDate.now().plusDays(4))
                .build();
    }

    public static BookingDto buildBookingDtoStayCantInAdvance() {

        return BookingDto.builder()
                .checkIn(LocalDate.now().plusDays(30))
                .checkOut(LocalDate.now().plusDays(31))
                .build();
    }

    public static BookingDto buildBookingDtoCheckinIsShorterThanLocalDateNow() {

        return BookingDto.builder()
                .checkIn(LocalDate.now().minusDays(5))
                .checkOut(LocalDate.now().minusDays(3))
                .build();
    }

    public static BookingDto buildBookingDtoCheckinIsNotShorterThanLocalDateNow() {

        return BookingDto.builder()
                .checkIn(LocalDate.now().plusDays(2))
                .checkOut(LocalDate.now().plusDays(3))
                .build();
    }

    public static ReservationDto buildReservationDto() {

        CustomerDto customer = new CustomerDto();
        customer.setName("Guilherme Antunes");
        customer.setEmail("antunes.guioliveira@gmail.com");
        customer.setSin("111-111-111");

        return ReservationDto.builder()
                .bookingDto(BookingDto.builder()
                        .checkIn(LocalDate.now())
                        .checkOut(LocalDate.now().plusDays(2))
                        .build())
                .customerDto(customer)
                .build();
    }

    public static ReservationEntity buildReservationEntity() {

        return ReservationEntity.builder()
                .bookingEntity(BookingEntity.builder()
                        .bookingId(1L)
                        .checkIn(LocalDate.now())
                        .checkOut(LocalDate.now().plusDays(2))
                        .build())
                .userEntity(UserEntity.builder()
                        .email("antunes.guioliveira@gmail.com")
                        .sin("111-111-111")
                        .name("Guilherme Antunes")
                        .build())
                .build();
    }

    public static BookingView buildBookingView() {
        return BookingView.builder()
                .bookingId(1L)
                .bedroom(new BedRoomView())
                .checkIn(LocalDate.now())
                .checkOut(LocalDate.now().plusDays(2))
                .user(new UserView())
                .build();
    }

    public static BookingView buildBookingViewCheckInBeforeThirtyDays() {
        return BookingView.builder()
                .bookingId(1L)
                .bedroom(new BedRoomView())
                .checkIn(LocalDate.now().minusDays(30))
                .checkOut(LocalDate.now().plusDays(2))
                .user(new UserView())
                .build();
    }

    public static UserView buildUserView() {
        return UserView.builder()
                .sin("111-111-111")
                .name("Guilherme Antunes")
                .userId(1L)
                .booking(List.of(buildBookingView()))
                .build();
    }

    public static AltenHotelEntity buildAltenHotelEntity() {

        AltenHotelEntity altenHotelEntity = new AltenHotelEntity();
        altenHotelEntity.setAltenHotelId(1L);
        altenHotelEntity.setBedroom(new BedRoomEntity());

        return altenHotelEntity;

    }

    public static BedRoomEntity buildBedRoomEntity() {

        BedRoomEntity bedRoomEntity = new BedRoomEntity();
        bedRoomEntity.setBedroomId(null);
        bedRoomEntity.setAltenHotel(buildAltenHotelEntity());
        bedRoomEntity.setBooking(null);

        return bedRoomEntity;
    }

    public static BookingEntity buildBookingEntity() {

        BookingEntity bookingEntity = new BookingEntity();
        BedRoomEntity bedRoomEntity = new BedRoomEntity();

        bedRoomEntity.setBedroomId(null);
        bedRoomEntity.setAltenHotel(buildAltenHotelEntity());

        bookingEntity.setBookingId(1L);
        bookingEntity.setCheckIn(LocalDate.now());
        bookingEntity.setCheckOut(LocalDate.now().plusDays(2));
        bookingEntity.setBedroom(bedRoomEntity);
        bookingEntity.setUser(buildReservationEntity().getUserEntity());

        return bookingEntity;
    }

    public static BookingEntity buildBookingEntityNew() {



        BookingEntity bookingEntity = new BookingEntity();
        BedRoomEntity bedRoomEntity = new BedRoomEntity();
        UserEntity userEntity = new UserEntity();

        userEntity.setUserId(1L);
        userEntity.setBooking(List.of(bookingEntity));
        userEntity.setSin("111-111-111");
        userEntity.setName("Guilherme Antunes");
        userEntity.setEmail("antunes.guioliveira@gmail.com");

        bedRoomEntity.setBedroomId(null);
        bedRoomEntity.setAltenHotel(buildAltenHotelEntity());

        bookingEntity.setBookingId(1L);
        bookingEntity.setCheckIn(LocalDate.now());
        bookingEntity.setCheckOut(LocalDate.now().plusDays(2));
        bookingEntity.setBedroom(bedRoomEntity);
        bookingEntity.setUser(userEntity);

        return bookingEntity;
    }

    public static UserEntity buildUserEntity() {

        UserEntity userEntity = new UserEntity();

        userEntity.setBooking(List.of(buildBookingEntity()));
        userEntity.setUserId(1L);
        userEntity.setEmail("antunes.guioliveira@gmail.com");
        userEntity.setName("Guilherme");
        userEntity.setSin("111-111-111");

        return userEntity;
    }
}
