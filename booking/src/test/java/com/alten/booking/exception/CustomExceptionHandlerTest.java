package com.alten.booking.exception;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.jpa.JpaObjectRetrievalFailureException;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class CustomExceptionHandlerTest {

    @InjectMocks
    private CustomExceptionHandler customExceptionHandler;

    @Mock
    private MethodArgumentNotValidException methodArgumentNotValidException;

    @Mock
    private JpaObjectRetrievalFailureException jpaObjectRetrievalFailureException;

    @Test
    @DisplayName("Handler booking rule exception")
    public void handler_booking_rule_exception() {

        BookingRuleException bookingRuleException = new BookingRuleException("Ops! The check-out can’t be equal or shorter than check-in");

        var exceptionDetails = customExceptionHandler.handlerBookingRuleException(bookingRuleException);

        assertEquals("Ops! The check-out can’t be equal or shorter than check-in", exceptionDetails.getMessage());
    }

    @Test
    @DisplayName("Period is not longer available")
    public void handler_booking_exception() {


        BookingException bookingException = new BookingException(
                "Opps! Sorry, but this period is no longer available, please try another check-in and checko-ut date");

        var exceptionDetails = customExceptionHandler.handlerBookingException(bookingException);

        assertEquals("Opps! Sorry, but this period is no longer available, please try another check-in and checko-ut date",
                exceptionDetails.getMessage());
    }

    @Test
    @DisplayName("Handler data does not exist")
    public void handler_empty_result_data_access_exception() {


        EmptyResultDataAccessException bookingException = new EmptyResultDataAccessException("Oops, this data does not exist", 0);

        var exceptionDetails = customExceptionHandler.handlerEmptyResultDataAccessException(bookingException);

        assertEquals("Oops, this data does not exist", exceptionDetails.getMessage());
    }

    @Test
    @DisplayName("Handler unexpected exception")
    public void handle_unexpected_exceptions() {

        Exception bookingException = new Exception("IOops, we had an internal problem in our system");

        var exceptionDetails = customExceptionHandler.handleUnexpectedExceptions(bookingException);

        assertEquals("Oops, we had an internal problem in our system", exceptionDetails.getMessage());
    }

    @Test
    @DisplayName("Handler payload not valid exception")
    public void handle_method_argument_not_valid_exception() {

        var exceptionDetails = customExceptionHandler.handleMethodArgumentNotValidException(methodArgumentNotValidException);

        assertEquals("Oops, i think you forgot to fill in some field", exceptionDetails.getMessage());
    }

    @Test
    @DisplayName("Handler update, data does not exist")
    public void handle_jpa_object_retrieval_failure_exception() {

        var exceptionDetails = customExceptionHandler.handleJpaObjectRetrievalFailureException(jpaObjectRetrievalFailureException);

        assertEquals("Oops, this data does not exist", exceptionDetails.getMessage());
    }
}
