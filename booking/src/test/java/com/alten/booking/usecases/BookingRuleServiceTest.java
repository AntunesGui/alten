package com.alten.booking.usecases;

import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.utils.Utils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class BookingRuleServiceTest {

    @InjectMocks
    private BookingRuleService bookingRuleService;

    @Mock
    private BookingRuleService bookingRuleServiceMock;

    @Test
    @DisplayName("Verify checkOut is greater than check-in")
    public void verify_checkOut_is_greater_than_checkIn_and_throw_bookingRuleException() {

        var bookingDto = Utils.buildBookingDtoCheckOutGreaterThanCheckIn();

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.bookingRule(bookingDto);
        });
    }

    @Test
    @DisplayName("Verify checkOut is equal or shorter than checkIn")
    public void verify_checkOut_is_equal_checkIn_and_throw_bookingRuleException() {

        var bookingDto = Utils.buildBookingDtoCheckOutEqualCheckIn();

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.bookingRule(bookingDto);
        });
    }

    @Test
    @DisplayName("Verify stay is not longer than time allowed")
    public void verify_stay_not_longer_than_the_time_allowed() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);

        var bookingDto = Utils.buildBookingDtoCheckOutLongerThanAllowed();

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.bookingRule(bookingDto);
        });
    }

    @Test
    @DisplayName("Verify stay cant be reserved in advance")
    public void verify_stay_cant_reserved_in_advance() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        var bookingDto = Utils.buildBookingDtoStayCantInAdvance();

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.bookingRule(bookingDto);
        });
    }

    @Test
    @DisplayName("Verify check-in is shorters and equals todays date")
    public void verify_checkin_is_shorter_and_equals_todays_date() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        var bookingDto = Utils.buildBookingDtoCheckinIsShorterThanLocalDateNow();

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.bookingRule(bookingDto);
        });
    }

    @Test
    @DisplayName("Verify check-in is valid")
    public void verify_checkin_is_valid() throws BookingRuleException {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        var bookingDto = Utils.buildBookingDtoCheckinIsNotShorterThanLocalDateNow();

        bookingRuleService.bookingRule(bookingDto);
    }

    @Test
    @DisplayName("Verify first check-in is after than thirty days")
    public void the_first_checkIn_is_after_thirty_days() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        assertFalse(bookingRuleService.checkIfTheFirstCheckInIsAfterThirtyDays(Utils.buildBookingView()));

    }

    @Test
    @DisplayName("Verify first check-in is before than thirty days")
    public void the_first_checkIn_is_before_thirty_days() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        assertTrue(bookingRuleService.checkIfTheFirstCheckInIsAfterThirtyDays(Utils.buildBookingViewCheckInBeforeThirtyDays()));

    }

    @Test
    @DisplayName("Testing days greater than 3 days")
    public void check_total_days_reserved() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.checkTotalDaysReserved(Utils.buildUserView(), Utils.buildReservationEntity());
        });
    }

    @Test
    @DisplayName("Testing reserved days greater than 3 days")
    public void check_reserved_days_greater_than_three() {

        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeLonger", 3);
        ReflectionTestUtils.setField(bookingRuleService, "daysCantBeReservedInAdvance", 29);

        var reservationEntity = Utils.buildUserView();
        reservationEntity.getBooking().get(0).setCheckOut(LocalDate.now().plusDays(3));

        assertThrows(BookingRuleException.class, () -> {
            bookingRuleService.checkTotalDaysReserved(reservationEntity, Utils.buildReservationEntity());
        });
    }
}
