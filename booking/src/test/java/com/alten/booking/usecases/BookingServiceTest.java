package com.alten.booking.usecases;


import com.alten.booking.domain.BookingView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.usecases.port.BookingCrudRepositoryPort;
import com.alten.booking.utils.Utils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class BookingServiceTest {

    @InjectMocks
    private BookingService bookingService;

    @Mock
    private ModelMapper mapper;

    @Mock
    private BookingCrudRepositoryPort repositoryPort;

    @Test
    @DisplayName("Making a reservation")
    public void making_reservation() throws BookingRuleException, BookingException {

        Integer one = 1;
        var reservationEntity = Utils.buildReservationEntity();
        var bookingView = Utils.buildBookingView();

        doNothing().when(repositoryPort).verifyAvailability(reservationEntity);
        doReturn(bookingView).when(repositoryPort).save(reservationEntity);

        BookingView response = bookingService.makingReservation(reservationEntity);

        verify(repositoryPort, times(one)).verifyAvailability(reservationEntity);
        assertEquals(bookingView.getBookingId(), response.getBookingId());
    }

    @Test
    @DisplayName("Update reservation")
    public void update_reservation() throws BookingException {

        Integer one = 1;
        var reservationEntity = Utils.buildReservationEntity();

        doNothing().when(repositoryPort).verifyAvailability(reservationEntity);
        doNothing().when(repositoryPort).update(reservationEntity, 1L);

        bookingService.updateReservation(reservationEntity, 1L);

        verify(repositoryPort, times(one)).verifyAvailability(reservationEntity);
        verify(repositoryPort, times(one)).update(reservationEntity, 1L);
    }

    @Test
    @DisplayName("Cancel reservation")
    public void cancel_reservation() {

        doNothing().when(repositoryPort).delete(1L);

        bookingService.cancelReservation(1L);

        verify(repositoryPort, times(1)).delete(1L);
    }

    @Test
    @DisplayName("Get available reservation")
    public void check_available_reservations() {

        Pageable pageable = PageRequest.of(0, 20);

        doReturn(List.of(Utils.buildBookingView())).when(repositoryPort).getAllReservations(pageable);

        var response = bookingService.getAllReservations(pageable);

        assertTrue(response.size() >= 1);
    }
}
