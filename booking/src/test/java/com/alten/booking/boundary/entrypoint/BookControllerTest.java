package com.alten.booking.boundary.entrypoint;

import com.alten.booking.boundary.entrypoint.mapper.ReservationMapper;
import com.alten.booking.domain.BookingView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.usecases.BookingRuleService;
import com.alten.booking.usecases.BookingService;
import com.alten.booking.utils.Utils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class BookControllerTest {

    @InjectMocks
    private BookController bookController;

    @Mock
    private BookingService bookingService;

    @Mock
    private ReservationMapper reservationMapper;

    @Mock
    private BookingRuleService bookingRuleService;

    @Test
    @DisplayName("Trying making reservation")
    public void trying_make_reservation() throws BookingRuleException, BookingException {


        var reservationDto = Utils.buildReservationDto();
        var reservationEntity = Utils.buildReservationEntity();
        var bookingView = Utils.buildBookingView();

        doNothing().when(bookingRuleService).bookingRule(any());
        doReturn(reservationEntity).when(reservationMapper).from(reservationDto);
        doReturn(bookingView).when(bookingService).makingReservation(reservationEntity);

        BookingView response = bookController.makeReservation(reservationDto);

        assertEquals(bookingView.getBookingId(), response.getBookingId());
    }

    @Test
    @DisplayName("Cancel reservation")
    public void trying_cancel_reservation() {

        lenient().doNothing().when(bookingService).cancelReservation(1L);

        bookController.cancelReservation(1L);
        verify(bookingService, times(1)).cancelReservation(1L);
    }

    @Test
    @DisplayName("Update reservation")
    public void trying_update_reservation() throws BookingRuleException, BookingException {

        var reservationDto = Utils.buildReservationDto();
        var reservationEntity = Utils.buildReservationEntity();

        doReturn(reservationEntity).when(reservationMapper).from(reservationDto);
        lenient().doNothing().when(bookingRuleService).bookingRule(any());
        lenient().doNothing().when(bookingService).updateReservation(reservationEntity, 1L);

        bookController.updateReservation(reservationDto, 1L);
        verify(bookingService, times(1)).updateReservation(reservationEntity, 1L);
    }

    @Test
    @DisplayName("Get available reservation")
    public void trying_check_available_reservations() {

        Pageable pageable = PageRequest.of(0, 20);

        doReturn(List.of(Utils.buildBookingView())).when(bookingService).getAllReservations(pageable);
        var response = bookController.checkAllAvailableReservations(pageable);

        assertTrue(response.getContent().size() >= 1);
    }
}
