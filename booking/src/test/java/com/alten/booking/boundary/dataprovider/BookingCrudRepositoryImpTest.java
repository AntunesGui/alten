package com.alten.booking.boundary.dataprovider;

import com.alten.booking.boundary.dataprovider.entity.*;
import com.alten.booking.boundary.dataprovider.implementation.BookingCrudRepositoryImp;
import com.alten.booking.boundary.dataprovider.repository.AltenHotelRepository;
import com.alten.booking.boundary.dataprovider.repository.BedRoomRepository;
import com.alten.booking.boundary.dataprovider.repository.BookingRepository;
import com.alten.booking.boundary.dataprovider.repository.UserRepository;
import com.alten.booking.boundary.entrypoint.mapper.BookingMapper;
import com.alten.booking.boundary.entrypoint.mapper.UserMapper;
import com.alten.booking.domain.BookingView;
import com.alten.booking.domain.UserView;
import com.alten.booking.exception.BookingException;
import com.alten.booking.exception.BookingRuleException;
import com.alten.booking.usecases.BookingRuleService;
import com.alten.booking.utils.Utils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class BookingCrudRepositoryImpTest {

    @InjectMocks
    private BookingCrudRepositoryImp bookingCrudRepositoryImp;

    @Mock
    private ModelMapper mapper;

    @Mock
    private UserMapper userMapper;

    @Mock
    private BookingMapper bookingMapper;

    @Mock
    private UserRepository userRepository;

    @Mock
    private BedRoomRepository bedRoomRepository;

    @Mock
    private BookingRepository bookingRepository;

    @Mock
    private BookingRuleService bookingRuleService;

    @Mock
    private AltenHotelRepository altenHotelRepository;

    @Test
    @DisplayName("Trying to create a reservation, when it is first time")
    public void save_when_user_not_present() throws BookingRuleException {

        var sin = "111-111-111";
        AltenHotelEntity altenHotelEntity = new AltenHotelEntity();
        BedRoomEntity bedRoomEntity = Utils.buildBedRoomEntity();
        UserEntity userEntity = Utils.buildReservationEntity().getUserEntity();
        BookingEntity bookingEntity = Utils.buildBookingEntity();
        BookingView bookingView = Utils.buildBookingView();


        doReturn(Optional.empty()).when(userRepository).findBySin(sin);
        doReturn(Utils.buildAltenHotelEntity()).when(altenHotelRepository).save(altenHotelEntity);
        doReturn(userEntity).when(userRepository).save(userEntity);
        doReturn(bedRoomEntity).when(bedRoomRepository).save(bedRoomEntity);
        doReturn(bookingEntity).when(bookingRepository).save(bookingEntity);
        doReturn(bookingView).when(bookingMapper).from(bookingEntity);

        var bookingViewResponse = bookingCrudRepositoryImp.save(Utils.buildReservationEntity());

        assertNotNull(bookingViewResponse);
    }

    @Test
    @DisplayName("Trying to create a reservation, when user is present and already has at least one reservation")
    public void save_when_user_is_present_and_has_at_least_one_reservation() throws BookingRuleException {

        var sin = "111-111-111";
        ReservationEntity reservationEntity = new ReservationEntity();

        UserEntity userEntity = new UserEntity();
        userEntity.setSin("111-111-111");
        userEntity.setBooking(List.of(new BookingEntity()));

        reservationEntity.setUserEntity(userEntity);
        reservationEntity.setBookingEntity(new BookingEntity());

        UserView userView1 = new UserView();
        userView1.setBooking(List.of(new BookingView()));

        BookingEntity bookingEntity1 = new BookingEntity();

        doReturn(Optional.of(userEntity)).when(userRepository).findBySin(sin);
        doReturn(userView1).when(userMapper).from(Optional.of(userEntity).get());
        doReturn(Optional.of(new BedRoomEntity())).when(bedRoomRepository).findById(userEntity.getUserId());
        doReturn(bookingEntity1).when(bookingRepository).save(reservationEntity.getBookingEntity());
        doReturn(new BookingView()).when(bookingMapper).from(bookingEntity1);

        var bookingViewResponse = bookingCrudRepositoryImp.save(reservationEntity);

        assertNotNull(bookingViewResponse);
    }

    @Test
    @DisplayName("Trying to create a reservation, when user is present and already has a reservation with more than thirty days")
    public void save_when_user_is_present_and_has_one_reservation_with_more_than_thirty_days() throws BookingRuleException {

        var sin = "111-111-111";
        ReservationEntity reservationEntity = new ReservationEntity();

        List<BookingEntity> bookingEntities = new ArrayList<>();

        bookingEntities.add(BookingEntity.builder()
                .bookingId(1L)
                .checkIn(LocalDate.now().plusDays(2))
                .checkOut(LocalDate.now().plusDays(1))
                .build());
        bookingEntities.add(BookingEntity.builder()
                .bookingId(2L)
                .checkIn(LocalDate.now().plusDays(4))
                .checkOut(LocalDate.now().plusDays(5))
                .build());

        UserEntity userEntity = new UserEntity();
        userEntity.setSin("111-111-111");
        userEntity.setBooking(bookingEntities);

        reservationEntity.setUserEntity(userEntity);
        reservationEntity.setBookingEntity(new BookingEntity());

        UserView userView1 = new UserView();
        userView1.setBooking(List.of(new BookingView()));

        BookingEntity bookingEntity1 = new BookingEntity();

        doReturn(Optional.of(userEntity)).when(userRepository).findBySin(sin);
        doReturn(userView1).when(userMapper).from(Optional.of(userEntity).get());
        doReturn(Optional.of(new BedRoomEntity())).when(bedRoomRepository).findById(userEntity.getUserId());
        doReturn(bookingEntity1).when(bookingRepository).save(reservationEntity.getBookingEntity());
        doReturn(new BookingView()).when(bookingMapper).from(bookingEntity1);
        doReturn(Boolean.TRUE).when(bookingRuleService).checkIfTheFirstCheckInIsAfterThirtyDays(userView1.getBooking().get(0));

        var bookingViewResponse = bookingCrudRepositoryImp.save(reservationEntity);

        assertNotNull(bookingViewResponse);
    }

    @Test
    @DisplayName("Trying to retirve all reservations available")
    public void get_all_reservations() {

        Pageable pageable = PageRequest.of(0, 5);
        Page<BookingEntity> bookingEntities = new PageImpl<>(List.of(new BookingEntity()), pageable, 1);

        doReturn(bookingEntities).when(bookingRepository).findAll(pageable);

        var bookingViewList = bookingCrudRepositoryImp.getAllReservations(pageable);

        assertTrue(bookingViewList.size() >= 1);
    }

    @Test
    @DisplayName("Trying to delete/cancel a reservation")
    public void cancel_reservation() {

        var bookingId = 1L;
        var times = 1;
        lenient().doNothing().when(bookingRepository).deleteById(bookingId);

        bookingCrudRepositoryImp.delete(bookingId);

        verify(bookingRepository, times(times)).deleteById(bookingId);
    }

    @Test
    @DisplayName("Trying to update a reservation")
    public void update_reservation() throws BookingException {

        var bookingId = 1L;
        var times = 1;
        ReservationEntity reservationEntity = Utils.buildReservationEntity();
        var bookingEntityUpdate = Utils.buildBookingEntity();

        doReturn(bookingEntityUpdate).when(bookingRepository).getById(bookingId);

        bookingCrudRepositoryImp.update(reservationEntity, bookingId);

        verify(bookingRepository, times(times)).save(bookingEntityUpdate);
    }

    @Test
    @DisplayName("No dates available")
    public void verify_availability() {

        doReturn(1).when(bookingRepository).checkAllAvailableReservations(
                Utils.buildReservationEntity().getBookingEntity().getCheckIn(),
                Utils.buildReservationEntity().getBookingEntity().getCheckOut());

        assertThrows(BookingException.class, () -> bookingCrudRepositoryImp.verifyAvailability(Utils.buildReservationEntity()));
    }
}
